<?php

//connection parameters

if(isset($_POST['db']){
    $database = $_POST['db'];	
}

if(isset($_POST['user']){
    $user = $_POST['user'];	
}

if(isset($_POST['password']){
    $password = $_POST['password'];	
}

//you can remove the above if-block and hardcode the db, user and password below
//$database = 'your db name';
//$user = 'your db2 username';
//$password = 'your db2 password';

if(isset($_POST['sql']){
	$sql = $_POST['sql'];
}

if(!$database || !$user || !$password || !$sql){
	$outputJSON = '{"success": false,"error":"Parameter Missing"}';
} else{

	//setting up the connection
	$conn = db2_connect($database, $user, $password);

	if ($conn) {
		//connection successful

		//building the query
		$stmt = db2_prepare($conn, $sql);

		//executing the query
		$result = db2_execute($stmt);

		if($result){
			//Hurray!!!!

			//temp variables
			$midJSON = "";
			$counter = false;

			//building the JSON output
			$outputJSON = '{"success": true';
			if (stripos($sql, "select") !== false && stripos($sql, "from") !== false) { //checking if select query
				while($row = db2_fetch_object($stmt)){
					if(!$counter){
						$midJSON .= json_encode($row);
						$counter = true;
					} else{
						$midJSON .= ','.json_encode($row);
					}
				}
				$outputJSON .= ',"output":['.$midJSON.']}';
			} else{
				$outputJSON .= '}';
			}

		} else{

			//Query Execution Failed!!!
			$outputJSON = '{"success": false,"error":"Query Execution Failed"}';		

		}

		//closing the connection
		db2_close($conn);
	}
	else {
		//Oops... Connection Failed
		$outputJSON = '{"success": false,"error":"Connection Failed"}';
	}
}

//echo the output
echo $outputJSON;

?> 
