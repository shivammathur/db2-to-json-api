# DB2 to JSON API
REST API to export data from DB2 server as JSON using PHP.

### Requirements
* DB2 Express-C or higher edition.
* IBM_DB2 PECL Package (https://pecl.php.net/package/ibm_db2)
  * Follow these instructions on how to install a pecl extention (http://php.net/manual/en/install.pecl.php)

### How to use
* Make sure your DB2 server is running.
* To test the API run index.php file and enter database, username, password, and sql-query and output json will be returned.
* To integrate it change the database, username and password from $_POST variable to actual values. Then you can do a POST request to sql.php using curl or a HTML form with 'sql' as a post field in the header.
  * Follow this tutorial to learn to send a curl post request (https://davidwalsh.name/curl-post).  
